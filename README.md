# The Odin Project

These files serve to give you a sense of how to do, iff, you feel lost in solving the problem.

Don't copy and paste any of these projects, try to solve any problems by yourself.

## Table of Contents
- [Recipes](https://gitlab.com/jusqua/recipes)
- [Landing Page](https://gitlab.com/jusqua/landing-page)
- [Rock Paper Scissors](https://gitlab.com/jusqua/rock-paper-scissors)
- [Etch-a-Sketch](https://gitlab.com/jusqua/etch-a-sketch)
- [Calculator](https://gitlab.com/jusqua/calculator)
- [Sign-up Form](https://gitlab.com/jusqua/sign-up-form)
- [Admin Dashboard](https://gitlab.com/jusqua/admin-dashboard)
- [Library](https://gitlab.com/jusqua/library)
